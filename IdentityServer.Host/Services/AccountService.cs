﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using IdentityServer4.Services;
using ReactCore.Infrastructure;
using ReactCore.Models;

using Robin.Communication.UserAuthentication;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace ReactCore.Services
{
    public class AccountService : ServiceBase
    {

        //private readonly UserAuthenticationStub _userAuthenticationStub;
        //private readonly IIdentityServerInteractionService _interaction;
        //private readonly IEventService _events;

        //public AccountService(UserAuthenticationStub userAuthenticationStub,IIdentityServerInteractionService interaction,IEventService events)
        //{
        //    _userAuthenticationStub = userAuthenticationStub;
        //    _interaction = interaction;
        //    _events = events;
        //}


        List<User> Users = new List<User>()
        {
            new User(){Email = "mehr@yahoo.com" ,Password = "123456",TellNum = "989354503503" , ID = 1, BadLastestIPs = new List<string>() },
            new User(){Email = "saeed@yahoo.com",Password = "123456",TellNum = "989357654321" , ID = 2, BadLastestIPs = new List<string>() },
            new User(){Email = "nader@yahoo.com",Password = "123456",TellNum = "987654321321" , ID = 3, BadLastestIPs = new List<string>() },
            new User(){Email = "maral@gmail.com",Password = "123456",TellNum = "989123456789" , ID = 4, BadLastestIPs = new List<string>() },
        };
        public enum LoginType
        {
            None, // ghalate
            Tell,// ok
            TellNotValid,// not ok 
            Email,//ok
            EmailNotValid,//not ok

        }
        public async Task<Result<ServiceUser>> Login(HttpContext context, LoginModel userModel)
        {

            User tmpUser = new User();

            if (userModel.Email == "" || userModel.Email == null)
            {//tell


                tmpUser = Users
                .Where(U => U.TellNum == userModel.TellNumber)
                .FirstOrDefault();
                userModel.Login = userModel.TellNumber;

            }
            else
            {//email

                //tmpUser = Users
                //.Where(U => U.Email == userModel.Email)
                //.FirstOrDefault();
                //userModel.Login = userModel.Email;

            }

            if (tmpUser.Password == userModel.Password)
            {
                try
                {
                    context.Response.Cookies.Append(Constants.AuthorizationCookieKey, userModel.Login); //TODO:Mehrshad
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                return Ok(new ServiceUser
                {
                    Login = userModel.Login
                });
            }
            else
                return Error<ServiceUser>("not");
        }

        public async Task<Result<ServiceUser>> Logon(HttpContext context, string TellnumOREmail)
        {
            try
            {
                if (IsNumber(TellnumOREmail))
                {
                    bool resGetByTell = getUserByTellNumber(TellnumOREmail);
                    //bool resGetByTell = await LoginRequestWithPhoneNumber(TellnumOREmail);

                    if (resGetByTell)
                        return Ok(new ServiceUser() { Logon = "Tell" });
                    else
                        return Error<ServiceUser>("TellNotValid");
                }
                else
                {
                    if (IsValidEmail(TellnumOREmail))
                    {
                        bool resGetByMail = getUserByEmail(TellnumOREmail);

                        if (resGetByMail)
                            return Ok(new ServiceUser() { Logon = "Email" });
                        else
                            return Error<ServiceUser>("EmailNotValid");
                    }
                    else
                        return Error<ServiceUser>("None");
                }
            }
            catch
            {
                return Error<ServiceUser>("None");
            }
        }

        public Result<ServiceUser> Verify(HttpContext context)
        {
            var cookieValue = context.Request.Cookies[Constants.AuthorizationCookieKey];
            if (string.IsNullOrEmpty(cookieValue))
                return Error<ServiceUser>();
            return Ok(new ServiceUser
            {

                Login = cookieValue
            });
        }

        public Result Logout(HttpContext context)
        {
            context.Response.Cookies.Delete(Constants.AuthorizationCookieKey);
            return Ok();
        }

        bool IsNumber(string Number)
        {
            try
            {
                BigInteger i = 0;
                string s = Number;
                bool result = BigInteger.TryParse(s, out i);
                return result;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private bool getUserByEmail(string tellnumOREmail)
        {
            var tmp = Users
                .Where(U => U.Email == tellnumOREmail)
                .FirstOrDefault();
            if (tmp == null)
                return false;
            else
                return true;
        }

        private bool getUserByTellNumber(string tellnumOREmail)
        {
            var tmp = Users
                .Where(U => U.TellNum == tellnumOREmail)
                .FirstOrDefault();
            if (tmp == null)
                return false;
            else
                return true;
        }


    }

    public class User
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public string TellNum { get; set; }
        public List<string> BadLastestIPs = new List<string>();
        public string Password { get; set; }
    }
}
