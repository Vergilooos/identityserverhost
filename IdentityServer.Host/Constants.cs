﻿namespace ReactCore
{
    public static class Constants
    {
        public static string AuthorizationCookieKey => "Auth";
        public static string HttpContextServiceUserItemKey => "ServiceUser";
        public static string OrderIDtestMehr => "OrderIDtestMehr";
    }
}
