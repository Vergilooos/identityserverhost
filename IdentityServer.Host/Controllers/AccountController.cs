﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Security.Claims;
using System.Threading.Tasks;
using Host.Quickstart.Register;
using IdentityModel;
using IdentityServer4.Quickstart.UI;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ReactCore.Infrastructure;
using ReactCore.Models;
using ReactCore.Services;
using Robin.Communication.UserAuthentication;
using Robin.Communication.UserRegisteration;

namespace ReactCore.Controllers
{
    // [SecurityHeaders]
    // [AllowAnonymous]
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly RegisterService _registerService;
        private readonly UserAuthenticationStub _userAuthenticationStub;
        private readonly IIdentityServerInteractionService _interaction;
        private readonly IEventService _events;

        public AccountController(UserRegisterationStub userRegisterationStub,
            UserAuthenticationStub userAuthenticationStub,
            IIdentityServerInteractionService interaction,
            IEventService events, AccountService accountService)
        {
            _registerService = new RegisterService(userRegisterationStub);
            _userAuthenticationStub = userAuthenticationStub;
            _interaction = interaction;
            _events = events;
            AccountService = accountService;
        }
        private AccountService AccountService { get; set; }

        //public AccountController(AccountService accountService)
        //{
        //    AccountService = accountService;
        //}

        //[ValidateAntiForgeryToken]
        [HttpPost("[action]")]
        public async Task<IActionResult> Logon(string TellnumOREmail)
        {
            var result = await Logon(HttpContext, TellnumOREmail);
            return Json(result);
        }
        [HttpPost("[action]")]
        public IActionResult Login([FromBody] LoginModel model)
        {
            LoginClassData loginClassData = new LoginClassData()
            {
                Context = HttpContext,
                userModel = model
            };
            //var result = LoginFunction(HttpContext, model);
            var result = LoginFunction(loginClassData);
            return Json(result);
        }
        [HttpPost("[action]")]
        public IActionResult Logout()
        {
            var result = AccountService.Logout(HttpContext);
            return Json(result);
        }
        ////////////////////////////////////////////
        //public async Task<Result<ServiceUser>> LoginFunction(HttpContext context, LoginModel userModel)
        public async Task<Result<ServiceUser>> LoginFunction(LoginClassData loginClassData)
        //  public async Task<Result<ServiceUser>> LoginFunction(HttpContext context)
        {
            try
            {
                //mehrtest
                var xxx = loginClassData.Context.Response.Cookies.ToString();// (Constants.OrderIDtestMehr, resulttmp.OrderID);
                //
                User tmpUser = new User();

                if (loginClassData.userModel.Email == "" || loginClassData.userModel.Email == null)
                {//tell
                    UserRegisterationResponse resultResponse = await _registerService.ValidatePhone(loginClassData.userModel.OrderID, loginClassData.userModel.Password);
                    if (resultResponse.IsSuccessful)
                    {//ok
                        List<Claim> claims = new List<Claim>();
                        claims.Add(new Claim(JwtClaimTypes.NickName, "DisplayName"));
                        claims.Add(new Claim(JwtClaimTypes.GivenName, "FirstName"));
                        claims.Add(new Claim(JwtClaimTypes.FamilyName, "LastName"));

                        var resultProfile = await _registerService.Profile(loginClassData.userModel.OrderID, claims);

                        if (resultProfile.IsError)
                        {

                        }
                    }
                    else
                    {//error

                    }
                }
                else
                {//email
                    UserRegisterationResponse result = await _registerService.ValidateEmail(loginClassData.userModel.OrderID, loginClassData.userModel.Password);

                }

                if (tmpUser.Password == loginClassData.userModel.Password)
                {
                    loginClassData.Context.Response.Cookies.Append(Constants.AuthorizationCookieKey, loginClassData.userModel.Login); //TODO:Mehrshad

                    Result<ServiceUser> result = new Result<ServiceUser>() { Value = new ServiceUser() { Login = loginClassData.userModel.Login, OrderID = loginClassData.userModel.OrderID } };
                    return result;
                }
                else
                {
                    Result<ServiceUser> resultSU = new Result<ServiceUser>();
                    resultSU.Errors.Add("not");
                    return resultSU;
                }
            }
            catch (Exception e)
            {
                Result<ServiceUser> resultSU = new Result<ServiceUser>();
                resultSU.Errors.Add("None" + e.Message);
                return resultSU;
            }
        }
        public async Task<Result<ServiceUser>> Logon(HttpContext context, string TellnumOREmail)
        {
            try
            {
                if (IsNumber(TellnumOREmail))
                {
                    if (true)//valid Tell Number تعداد مثلا
                    {
                        LoginRequestWithPhoneResponse resGetByTell = await LoginRequestWithPhoneNumber(TellnumOREmail);
                        if (resGetByTell.IsSuccessful)
                        {
                            //login
                            Result<ServiceUser> result = new Result<ServiceUser>() { Value = new ServiceUser() { Logon = "Tell", OrderID = "" } };
                            return result;
                        }
                        else
                        {
                            if (resGetByTell.Error == "invalid_user")
                            {
                                //new user
                                var registerModel = new RegisterViewModel();
                                registerModel.RegisterType = RegisterViewModel.Type.Phone;
                                registerModel.Identifier = TellnumOREmail;

                                var resulttmp = await _registerService.ApplyAsync(registerModel.RegisterType, registerModel.Identifier);

                                if (resulttmp.IsSuccessful)
                                {
                                    //mehrtest
                                    context.Response.Cookies.Append(Constants.OrderIDtestMehr, resulttmp.OrderID);
                                    Result<ServiceUser> result = new Result<ServiceUser>() { Value = new ServiceUser() { Logon = "Tell", OrderID = resulttmp.OrderID } };
                                    return result;
                                }
                                else
                                {
                                    Result<ServiceUser> resultSU = new Result<ServiceUser>();
                                    resultSU.Errors.Add("None");
                                    return resultSU;
                                }
                            }
                            else
                            {
                                Result<ServiceUser> resultSU = new Result<ServiceUser>();
                                resultSU.Errors.Add("None");
                                return resultSU;
                            }
                        }
                    }
                    else
                    {
                        Result<ServiceUser> resultSU = new Result<ServiceUser>();
                        resultSU.Errors.Add("TellNotValid");
                        return resultSU;// Result.Error<ServiceUser>("TellNotValid");
                    }
                }
                else
                {
                    if (IsValidEmail(TellnumOREmail))
                    {
                        LoginRequestWithEmailResponse resGetByMail = await LoginRequestWithEmail(TellnumOREmail);
                        if (resGetByMail.IsSuccessful)
                        {
                            //login
                            Result<ServiceUser> result = new Result<ServiceUser>() { Value = new ServiceUser() { Logon = "Email", OrderID = "" } };
                            return result;
                        }
                        else
                        {
                            if (resGetByMail.Error == "invalid_user")
                            {
                                //new user
                                var registerModel = new RegisterViewModel();
                                registerModel.RegisterType = RegisterViewModel.Type.Email;
                                registerModel.Identifier = TellnumOREmail;

                                var resulttmp = await _registerService.ApplyAsync(registerModel.RegisterType, registerModel.Identifier);

                                if (resulttmp.IsSuccessful)
                                {
                                    //mehrtest
                                    context.Response.Cookies.Append(Constants.OrderIDtestMehr, resulttmp.OrderID);

                                    Result<ServiceUser> result = new Result<ServiceUser>() { Value = new ServiceUser() { Logon = "Email", OrderID = resulttmp.OrderID } };
                                    return result;
                                }
                                else
                                {
                                    Result<ServiceUser> resultSU = new Result<ServiceUser>();
                                    resultSU.Errors.Add("None");
                                    return resultSU;
                                }
                            }
                            else
                            {
                                Result<ServiceUser> resultSU = new Result<ServiceUser>();
                                resultSU.Errors.Add("None");
                                return resultSU;
                            }
                        }
                    }
                    else
                    {
                        Result<ServiceUser> resultSU = new Result<ServiceUser>();
                        resultSU.Errors.Add("EmailNotValid");
                        return resultSU;
                    }
                }
            }
            catch (Exception e)
            {
                Result<ServiceUser> resultSU = new Result<ServiceUser>();
                resultSU.Errors.Add("None" + e.Message);
                return resultSU;
            }
        }
        bool IsNumber(string Number)
        {
            try
            {
                BigInteger i = 0;
                string s = Number;
                bool result = BigInteger.TryParse(s, out i);
                return result;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }


        ////////////////////////////////////
        ///from nader Inden
        ///////////////////////////////////

        private async Task<LoginRequestWithPhoneResponse> LoginRequestWithPhoneNumber(string phoneNumber)
        {
            LoginRequestWithPhoneResponse result = await _userAuthenticationStub.Get().LoginRequestWithPhoneAsync(
                    new LoginRequestWithPhoneRequest()
                    {
                        PhoneNumber = phoneNumber
                    });
            //if (result.IsSuccessful)
            //    ModelState.AddModelError(string.Empty, result.Message);
            //else
            //    ModelState.AddModelError(string.Empty, result.ErrorDescription);

            return result;
        }

        private async Task<LoginRequestWithEmailResponse> LoginRequestWithEmail(string email)
        {
            LoginRequestWithEmailResponse result = await _userAuthenticationStub.Get().LoginRequestWithEmailAsync(
                    new LoginRequestWithEmailRequest()
                    {
                        Email = email
                    });
            //if (result.IsSuccessful)
            //    ModelState.AddModelError(string.Empty, result.Message);
            //else
            //    ModelState.AddModelError(string.Empty, result.ErrorDescription);

            return result;

        }

        private async Task<ValidateCredentialResponse> ValidateCredentialsViaPhoneNumber(string phoneNumber, string code)
        {
            return await _userAuthenticationStub
                .Get()
                .ValidateCredentialsViaPhoneNumberAsync(
                    new ValidateCredentialRequest()
                    {
                        Identifire = phoneNumber,
                        Secret = code
                    });
        }

        private async Task<ValidateCredentialResponse> ValidateCredentialsViaEmail(string Email, string code)
        {
            return await _userAuthenticationStub.Get().ValidateCredentialsViaEmailAsync(
                    new ValidateCredentialRequest()
                    {
                        Identifire = Email,
                        Secret = code
                    });
        }
        private async Task<ValidateCredentialResponse> ValidateCredentialsViaUserpass(string username, string password)
        {
            return await _userAuthenticationStub.Get().ValidateCredentialsViaUserpassAsync(
                   new ValidateCredentialRequest()
                   {
                       Identifire = username,
                       Secret = password
                   });
        }
    }



}
