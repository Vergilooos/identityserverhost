import { render } from "react-dom";
import React, { useState, useCallback, useEffect } from "react";
import { useTransition, animated } from "react-spring";
import LoginComp from "../components/LoginComp/LoginComp";
import "@Components/LoginComp/LoginStyle.scss";
import Logo from "@Assets/img/Logo/Logo.jpg";
import ActivationComp from "../components/LoginComp/ActivationComp";

export default function LoginPage33(props) {
  const pages = [
    ({ style }) => (
      <animated.div style={{ ...style }}>
        <LoginComp onClick={(value) => { props.onClickPhoneOREmail(value) }} />
      </animated.div>
    ),
    ({ style }) => (
      <animated.div style={{ ...style }}>
        <ActivationComp onClick={(value) => { props.onClickActivationCodeButtun(value) }} />
      </animated.div>
    ),
    // ({ style }) => (
    //   <animated.div style={{ ...style, background: "lightgreen" }}>
    //     C
    //   </animated.div>
    // ),
  ];
  useEffect(() => {
    // Update the document title using the browser API
    if (props.gotoActivation) {
      props.FalsegotoActivation();
      onClick();
    }
  });
  const [index, set] = useState(0);
  const onClick = useCallback(() => set((state) => (state + 1) % 2), []);
  const transitions = useTransition(index, (p) => p, {
    //   from: { opacity: 0, transform: "translate3d(100%,0,0)" },
    from: { opacity: 0, transform: "translate3d(0%,0,0)" },
    //enter: { opacity: 1, transform: "translate3d(0%,0,0)" },
    enter: { opacity: 1 },
    leave: { opacity: 0, transform: "translate3d(-50%,0,0)" },
  });
  return (
    <div className="Container BoxSize">
      <div className="LogoContainer">
        <img src={Logo} className="Logoimg" />
      </div>

      <div
        className="simple-trans-main"
      // onClick={onClick}
      >
        {transitions.map(({ item, props, key }) => {
          const Page = pages[item];
          return <Page key={key} style={props} />;
        })}
      </div>
    </div>
  );
  return (
    <div className="Container">
      <div className="simple-trans-main" onClick={onClick}>
        {transitions.map(({ item, props, key }) => {
          const Page = pages[item];
          return <Page key={key} style={props} />;
        })}
      </div>
    </div>
  );
}

// render(<LoginPage />, document.getElementById("root"));
