import * as React from "react";
import { Helmet } from "react-helmet";
import './HomePageStyle.scss';
import Logo from "@Assets/img/Logo/Logo.jpg";
import '@Styles/Loading.scss';
export default class HomePage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <Helmet><title>Robbin</title></Helmet>
        <div className="MainContainer ">
          <div className="Container BoxSize">
            <div className="LogoContainer">
              <img src={Logo} className="Logoimg" />

            </div>
            <p id="Text">شما با موفقیت وارد شدید</p>
            <div className="loader">ads</div>
            <p id="ٌWaitText">در حال انتقال به گفتمان لطفا صبر کنید</p>
          </div>
        </div>
      </React.Fragment>

    );
  }
}
