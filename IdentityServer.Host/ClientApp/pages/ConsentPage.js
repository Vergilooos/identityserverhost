import React from 'react';
import "./ConsentStyle.scss";
import './HomePageStyle.scss';
import { Helmet } from "react-helmet";
import CheckBox from '../components/CheckBox/CheckBox';
import ConsentRow from '../components/ConstentRow/ConsentRow';
import { Row,Col } from 'reactstrap';
import '@Styles/PerfectScrollBar.scss';
import PerfectScrollbar from 'react-perfect-scrollbar'
import ApprovePermission from '../components/ApprovePermision/ApprovePermition';
import { Route, Redirect,withRouter,observer } from "react-router-dom";
 export default class ConsentPage extends React.Component{
  state={
   ImageProfile:false,
   ApprovePermision:false
  }  
  handleInputChange=(rr)=>{
    
    this.setState({isGoing:!this.state.isGoing})
  }
  renderRow=()=>{
    let temp = [];
   for(var i=0;i<10;i++){
    
  temp.push(  <ConsentRow Content={"اجازه دسترسی به عکس پروفایل"} Value={(res)=>this.setState({ImageProfile:res})}  />);
  
   }
   return temp;
  }
  onClick=()=>{
     if(this.state.ApprovePermision){
      this.props.history.push("/")
     // return <Redirect  to="/" />
    }
  }
  render(){
        return(
            <React.Fragment>
            <Helmet><title>Robbin</title></Helmet>
            <div className="MainContainer ">
              <div className="Container BoxSize">
               <h2 id="h2">دسترسی ها </h2>
               <div style={{height:'75%', width:'100%' ,overflow: 'auto'}}>
               <PerfectScrollbar>
              
                {this.renderRow()}
                <ApprovePermission Checked={(res)=>{this.setState({ApprovePermision:res});}} onClick={()=>{this.onClick()}} />
                </PerfectScrollbar>
                </div>
               </div>
            </div>
            </React.Fragment>
        )
    }
}
