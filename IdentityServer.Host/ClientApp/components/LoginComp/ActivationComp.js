import React from "react";
import "./LoginStyle.scss";
import Logo from "@Assets/img/Logo/Logo.jpg";
import { useTransition, animated } from "react-spring";
import { Form } from "@Components/shared/Form";
import "@Styles/main.scss";
import bind from "bind-decorator";
export default class ActivationComp extends React.Component {
 state={
   value:""
 }
  handleChange=(event)=> {
    this.setState({value: event.target.value});
  } 
  render() {
    return (
      <div className="LoginCompContainer">
        <h2>کد فعالسازی</h2>
        <div class="form-group  w-100" style={{ width: "80%" }}>
          {/* <Form ref={(x) => (this.elForm = x)}> */}
            <div className="form-group d-flex justify-content-center flex-column">
              <label htmlFor="inputLogin">کد فعالسازی</label>
              <input
                type="text"
                name="login"
                data-value-type="string"
                className="form-control"
                id="inputLogin"
                data-val-required="true"
                data-msg-required="Login is required."
                value={this.state.value} 
                onChange={this.handleChange}
              />
            </div>

            <div className="form-inline Buttuncontainer">
              <button
                className="btn btn-success  ButtunStyle"
                onClick={()=>this.props.onClick(this.state.value)}
              >
               ورود
              </button>
            </div>
          {/* </Form> */}
        </div>
        {/* <animated.div className="Logo" /> */}
        {/* <img src={Logo} className="Logo" /> */}
        {/* <h2>We don't mind keeping you posted!</h2>
        <p>
          Twice a week, we send in warm regards with rich contents from around
          the web.
        </p> */}
      </div>
    );
  }
}
