import React from 'react';
import CheckBox from '../CheckBox/CheckBox';
import './ConsentRowStyle.scss';
export default function ConsentRow (props){
    
    return(
        <div id="Wrapper" >
            <div id="RowContainer">
            <p id="TextStyle">
            {props.Content}
            </p>
            <div id="CheckBox">
            <CheckBox Value={(res)=>{props.Checked(res);}} />
            </div>
            </div>
        </div>
    )
}