import { Route, Redirect } from "react-router";
import * as React from "react";
import Globals from "@Globals";


const AppRoute = ({
  component: Component,
  layout: Layout,
  path: Path,
  ...rest
}) => {
  var isLoginPath = Path === "/login";
  if (!Globals.isAuthenticated && !isLoginPath) {
    return <Redirect to="/Login" />;
  }

  if (Globals.isAuthenticated && isLoginPath) {
    if (localStorage.getItem('Consent')) {

      alert(localStorage.getItem('Consent').toString());
      return <Redirect to="/" />;
    }
    else {
      localStorage.setItem('Consent', "true");
      <Redirect to="/Consents" />
    }
  }


  return (
    <Route
      {...rest}
      render={(props) => (
        <Layout {...props} >
          <Component {...props} />
        </Layout>

      )}
    />
  );
};

export default AppRoute;
