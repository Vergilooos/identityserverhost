import React ,{ useState }  from 'react';
export default function CheckBox(props){
    const [Checked,setChecked] = useState(false);
    return(
    <input
    name="isGoing"
    type="checkbox"
    checked={Checked}
    onChange={()=>{setChecked(!Checked);props.Value(Checked)}} 
   
    />
)
}