import React from 'react';
import CheckBox from '../CheckBox/CheckBox';
import './ApproveStyle.scss';
export default function ApprovePermission(props){
    return(
        <React.Fragment>
        <div id="ApproveContainer">
            <div id="Content">
            <p id="ApproveText" >اجازه دسترسی گفتمان به اطلاعات بالا را تایید میکنم</p>
            </div>
            <div id="CheckBox">
            <CheckBox Value={(res)=>{props.Checked(!res);}} />
            </div>
        </div> 
        <div id="ButtunContainer">
           <button
           id="ButtunApprove"
           className="btn btn-success  "
           onClick={()=>props.onClick()}
         >
          تایید
         </button>
         </div> 
         </React.Fragment>
    )
}