﻿import TopMenu from "@Components/shared/TopMenu";
import * as React from "react";
import { ToastContainer } from "react-toastify";
import Footer from "@Components/shared/Footer";
import { Container, Row, Col } from 'reactstrap';
export default class AuthorizedLayout extends React.Component {
    componentDidMount() {
        // let res = localStorage.getItem("RTL");
        // if (res == null) {
        //     localStorage.setItem("RTL", "true");
        // }
    }
    render() {
        return (
            <div rtl='true' style={{ width: '100%', height: '100%', backgroundColor: 'white' }}>
                {this.props.children}
            </div>
            // <Layout >
            //     {this.props.children}
            // </Layout>
        )
        return (
            <Container fluid className="contain">
                <Row>
                    <Col className="TopBar" sm="12">
                        {/* <TopBar /> */}
                    </Col>
                </Row>
                <Row>
                    <Col xs="0" sm="2" className="SideBar" >

                    </Col>
                    <Col xs="12" sm="10" className="Body">
                        {/* Aenean ut felis finibus, aliquet mi a, feugiat felis. Donec porta, odio et vulputate laoreet, nibh odio iaculis mi, et ornare nulla orci vitae ligula. Sed mi velit, aliquam sit amet efficitur eget, scelerisque vel ligula. Aliquam finibus erat nec accumsan posuere. Vestibulum rhoncus, velit vitae volutpat vehicula, leo orci faucibus eros, at ornare nibh nunc nec mi. Donec porttitor ultricies mauris quis euismod. Praesent sem libero, venenatis ut ornare eget, volutpat tincidunt lacus. Pellentesque aliquam turpis et mauris consectetur, quis condimentum nunc dignissim. Cras lectus libero, pellentesque non malesuada at, condimentum nec ex. Nam sed accumsan enim. Donec eros massa, malesuada quis nulla elementum, imperdiet condimentum orci. Integer non velit et nulla vestibulum vestibulum. Proin vehicula tristique libero, eu tincidunt erat cursus ac. Ut malesuada ante ut est dictum, ornare varius arcu aliquet. Quisque vitae libero eget orci tristique aliquam id sit amet nunc. */}
                        <Row >
                            <Col sm='12' className="Footer" ></Col>
                        </Row>

                    </Col>
                </Row>

            </Container>
        )
        return <div id="authorizedLayout" className="layout">
            <TopMenu />
            <div className="container container-content">
                {this.props.children}
            </div>
            <ToastContainer />
            <Footer />
        </div>;
    }
}