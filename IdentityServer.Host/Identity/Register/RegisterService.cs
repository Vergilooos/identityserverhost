﻿using Robin.Communication.UserRegisteration;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Host.Quickstart.Register
{
    public class RegisterService
    {
        //TODO: grpc inputs must be not null. throw exception
        private readonly UserRegisterationStub _userRegisterationStub;
        public RegisterService(UserRegisterationStub userRegisterationStub)
        {
            _userRegisterationStub = userRegisterationStub;
        }
        public async Task<UserRegisterationResponse> ApplyAsync(RegisterViewModel.Type type, string identifier)
        {
            var request = new ApplyRequest()
            {
                Identifier = identifier
            };

            switch (type)
            {
                case RegisterViewModel.Type.Email:
                    request.RequestType = RegisterationType.Email;
                    break;
                case RegisterViewModel.Type.Phone:
                    request.RequestType = RegisterationType.Phone;
                    break;
                case RegisterViewModel.Type.UserPass:
                    request.RequestType = RegisterationType.UserPass;
                    break;
                default:
                    return null;
            }

            return await _userRegisterationStub.Get().ApplyAsync(request);
        }

        public async Task<UserRegisterationResponse> ValidatePhone(string orderID, string code)
        {
            return await _userRegisterationStub.Get().ValidatePhoneAsync(new ValidateIdentifierRequest()
            {
                OrderID = orderID,
                Secret = code
            });
        }

        public async Task<UserRegisterationResponse> ValidateEmail(string orderID, string code)
        {
            return await _userRegisterationStub.Get().ValidateEmailAsync(new ValidateIdentifierRequest()
            {
                OrderID = orderID,
                Secret = code
            });
        }

        public async Task<UserRegisterationResponse> ValidatePassword(string orderID, string code)
        {
            return await _userRegisterationStub.Get().ValidatePasswordAsync(new ValidateIdentifierRequest()
            {
                OrderID = orderID,
                Secret = code
            });
        }

        public async Task<LoginResult> Profile(string orderID, List<Claim> claims)
        {
            var request = new ProfileInformationRequest()
            {
                OrderID = orderID
            };
            foreach (var claim in claims)
            {
                request.Claims.Add(new KeyValue()
                {
                    Key = claim.Type,
                    Value = claim.Value
                });
            }
            return await _userRegisterationStub.Get().ProfileInformationAsync(request);
        }

        public async Task<LoginResult> ExternalLogin(IEnumerable<Claim> claims, string providerName)
        {
            var request = new ExternalLoginRequest()
            {
                ProviderName = providerName,
            };
            foreach (var claim in claims)
            {
                request.Claims.Add(new KeyValue()
                {
                    Key = claim.Type,
                    Value = claim.Value
                });
            }
            return await _userRegisterationStub.Get().ApplyExternalLoginAsync(request);
        }
    }
}
