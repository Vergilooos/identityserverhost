﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Host.Quickstart.Register
{
    public class RegisterViewModel
    {
        public string OrderID { get; set; }
        public enum Type
        {
            Email,
            Phone,
            UserPass,
            Google
        }
        public Type RegisterType { get; set; }
        public string NextState { get; set; }
        public string Identifier { get; set; }
        public string Credential { get; set; }
        public string DisplayName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
