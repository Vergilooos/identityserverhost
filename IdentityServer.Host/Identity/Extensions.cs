using System;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Robin.Utilities.EmailService;
using Robin.Utilities.SMSService;

namespace IdentityServer4.Quickstart.UI
{
    public static class Extensions
    {
        /// <summary>
        /// Checks if the redirect URI is for a native client.
        /// </summary>
        /// <returns></returns>
        public static bool IsNativeClient(this AuthorizationRequest context)
        {
            return !context.RedirectUri.StartsWith("https", StringComparison.Ordinal)
               && !context.RedirectUri.StartsWith("http", StringComparison.Ordinal);
        }

        public static IActionResult LoadingPage(this Controller controller, string viewName, string redirectUri)
        {
            controller.HttpContext.Response.StatusCode = 200;
            controller.HttpContext.Response.Headers["Location"] = "";

            return controller.View(viewName, new RedirectViewModel { RedirectUrl = redirectUri });
        }

        public static IServiceCollection AddUtilities(this IServiceCollection services, IConfigurationSection emailConfiguration)
        {
            //add email service
            services.AddSingleton(emailConfiguration.Get<EmailConfiguration>());
            services.AddScoped<IEmailSender, EmailSender>();

            //add sms service
            //utilities
            services.AddScoped<ISMSSender, SMSSender>();
            return services;
        }
    }
}
