﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactCore.Models
{
    public class ServiceUser
    {
        public string Login { get; set; }
        public string Logon { get; set; }
        public string OrderID { get; set; }
    }
    public class LoginClassData
    {
        public HttpContext Context { get; set; }
        public LoginModel userModel { get; set; }

    }
}
