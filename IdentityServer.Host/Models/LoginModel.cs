﻿namespace ReactCore.Models
{
    public class LoginModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public string Email { get; set; }
        public string TellNumber { get; set; }
        public string OrderID { get; set; }
    }
}
