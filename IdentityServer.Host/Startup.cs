using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;

using Microsoft.AspNetCore.Authentication.Certificate;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ReactCore.Infrastructure;
using ReactCore.Services;
using Serilog;
using IdentityServer4.Endpoints.Communication;
using Robin.Communication.Extentions;
using Robin.IdentityServer.IdentityServer.Endpoints.Communication;
using System.Collections.Generic;
using Robin.Communication.UserRegisteration;
using Robin.Communication.UserAuthentication;
using IdentityServer4.Quickstart.UI;
using IdentityServer4;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace ReactCore
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            Configuration.GetSection("AppSettings").Bind(AppSettings.Default);

            services.AddAuthentication(CertificateAuthenticationDefaults.AuthenticationScheme).AddCertificate();
            services.AddControllersWithViews();

            //IdentityServer configs
            var connectionString = Configuration.GetConnectionString("db");
            var useCache = Configuration.GetValue<bool>("CacheSettings:UseRedis");

            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = builder => builder.UseSqlServer(connectionString);
                }, useCache)
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = builder => builder.UseSqlServer(connectionString);

                    // this enables automatic token cleanup. this is optional.
                    options.EnableTokenCleanup = true;
                    options.TokenCleanupInterval = 60; // interval in seconds, short for testing
                }, useCache)
                .AddUserStore(options =>
                {
                    options.ConfigureDbContext = builder => builder.UseSqlServer(connectionString);
                }, useCache)
                .AddRegisterationCodeStore(useCache)
                .AddRedisCache(options =>
                {
                    options.Nodes = new List<KeyValuePair<string, int>>
                    {
                        new KeyValuePair<string, int>(
                            Configuration.GetSection("RedisConfiguration:node").Value,
                            int.Parse(Configuration.GetSection("RedisConfiguration:port").Value))
                    };
                }, useCache);
            services.AddUtilities(Configuration.GetSection("EmailConfiguration"));
            services.AddCommunicationServer()
                .AddCommunicationService<ClientManagementService>()
                .AddCommunicationService<UserRegisterationService>()
                .AddCommunicationService<UserAuthenticationService>()
                .AddCommunicationClient<UserRegisterationStub>()
                .AddCommunicationClient<UserAuthenticationStub>();
            //end of IdentityServer configs

            services.AddAuthentication()
               .AddOpenIdConnect("Google", "Google", options =>
               {
                   options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
                   options.ForwardSignOut = IdentityServerConstants.DefaultCookieAuthenticationScheme;

                   options.Authority = "https://accounts.google.com/";
                   options.ClientId = "948092757627-aqh4up3q4eerusag0enih3th1j8jk8sl.apps.googleusercontent.com";
                   options.ClientSecret = "ds0hJf1Ran6FdoY5QTU9GgYg";

                   options.CallbackPath = "/signin-google";
                   options.Scope.Add("email");
               });
            services.AddHttpsRedirection(options =>
            {
                options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
                options.HttpsPort = 443;
            });


            services.AddLogging(loggingBuilder =>
                loggingBuilder
                    .AddSerilog(dispose: true)
                    .AddAzureWebAppDiagnostics()
                );

            services.AddRazorPages();
            services.AddNodeServices();
            services.AddSpaPrerenderer();

            // Add your own services here.
            services.AddScoped<AccountService>();
            services.AddScoped<PersonService>();

            return services.BuildServiceProvider();
        }

        //public IServiceProvider ConfigureServices(IServiceCollection services)
        //{
        //    Configuration.GetSection("AppSettings").Bind(AppSettings.Default);

        //    services.AddLogging(loggingBuilder =>
        //        loggingBuilder
        //            .AddSerilog(dispose: true)
        //            .AddAzureWebAppDiagnostics()
        //        );
        //    services.AddRazorPages();
        //    services.AddNodeServices();
        //    services.AddSpaPrerenderer();

        //    // Add your own services here.
        //    services.AddScoped<AccountService>();
        //    services.AddScoped<PersonService>();

        //    return services.BuildServiceProvider();
        //}
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        //public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        //{
        //    app.UseMiddleware<ExceptionMiddleware>();
        //    app.UseMiddleware<Host.Logging.RequestLoggerMiddleware>();

        //    // Build your own authorization system or use Identity.
        //    app.Use(async (context, next) =>
        //    {
        //        var accountService = (AccountService)context.RequestServices.GetService(typeof(AccountService));
        //        var verifyResult = accountService.Verify(context);
        //        if (!verifyResult.HasErrors)
        //        {
        //            context.Items.Add(Constants.HttpContextServiceUserItemKey, verifyResult.Value);
        //        }
        //        await next.Invoke();
        //        // Do logging or other work that doesn't write to the Response.
        //    });

        //    if (env.IsDevelopment())
        //    {
        //        app.UseDeveloperExceptionPage();
        //        app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
        //        {
        //            HotModuleReplacement = true
        //        });
        //    }
        //    else
        //    {
        //        app.UseExceptionHandler("/Main/Error");
        //    }

        //    app.UseStaticFiles();
        //    app.UseRouting();
        //    app.UseEndpoints(endpoints =>
        //    {
        //        endpoints.MapControllerRoute("default", "{controller=Main}/{action=Index}/{id?}");
        //        endpoints.MapFallbackToController("Index", "Main");
        //    });
        //}

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<Host.Logging.RequestLoggerMiddleware>();

            app.UseMiddleware<ExceptionMiddleware>();

            // Build your own authorization system or use Identity.
            app.Use(async (context, next) =>
            {
                var accountService = (AccountService)context.RequestServices.GetService(typeof(AccountService));
                var verifyResult = accountService.Verify(context);
                if (!verifyResult.HasErrors)
                {
                    context.Items.Add(Constants.HttpContextServiceUserItemKey, verifyResult.Value);
                }
                await next.Invoke();
                // Do logging or other work that doesn't write to the Response.
            });

            //app.Use(async (context, next) =>
            //{
            //    context.Response.Headers.Add(
            //        "Content-Security-Policy",
            //        "img-src *;");

            //    await next();
            //});
            if (Configuration.GetValue<bool>("CacheSettings:UseRedis"))
                app.UseRedis();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Main/Error");
            }

            app.UseStaticFiles();
            app.UseRouting();

            app.UseIdentityServer();

            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "{controller=Main}/{action=Index}/{id?}");
                endpoints.MapFallbackToController("Index", "Main");
            });
            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapDefaultControllerRoute();
            //});

            app.AddCommunicationEndPoints<ClientManagementService>();
            app.AddCommunicationEndPoints<UserRegisterationService>();
            app.AddCommunicationEndPoints<UserAuthenticationService>();

        }
    }
}
